app "hello"
    packages { pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br" }
    imports [
        pf.Stdout,
        pf.Task.{ Task },
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str,
        "testinput2.txt" as testinput2 : Str,
    ]
    provides [main] to pf

isDigit : U8 -> Bool
isDigit = \b ->
    Bool.and (0x30 <= b) (b <= 0x39)

numFromLine : Str -> I64
numFromLine = \line ->
    digits =
        keepDigits line
        |> Str.toUtf8

    firstDigit : I64
    firstDigit =
        List.first digits
        |> Result.withDefault 30
        |> Num.toI64
        |> (\n -> n - 0x30)
        |> (\n -> n * 10)

    lastDigit : I64
    lastDigit =
        List.last digits
        |> Result.withDefault 30
        |> Num.toI64
        |> (\n -> n - 0x30)

    firstDigit + lastDigit

expect numFromLine "1a2b3" == 13
expect numFromLine "abcd990abc9d" == 99
expect numFromLine "a00000a" == 0
expect numFromLine "1234567890" == 10
expect numFromLine "1abc2" == 12
expect numFromLine "pqr3stu8vwx" == 38
expect numFromLine "a1b2c3d4e5f" == 15
expect numFromLine "treb7uchet" == 77

keepDigits : Str -> Str
keepDigits = \in ->
    Str.walkUtf8
        in
        ""
        (\a, b ->
            when Str.fromUtf8 [b] is
                Ok bstr ->
                    if isDigit b then
                        Str.concat a bstr
                    else
                        a

                Err _ -> a
        )
expect keepDigits "ab1cd2ef3gh" == "123"

solvePartOne : Str -> I64
solvePartOne = \in ->
    in
    |> Str.trim
    |> Str.split "\n"
    |> List.map numFromLine
    |> List.sum
expect solvePartOne testinput == 142
expect solvePartOne input == 55488

## part two

digitwords : List Str
digitwords = [
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
]

startsWithDigitWord : Str -> Result I64 [NoDigit]
startsWithDigitWord = \line ->
    result : I64
    result = List.walkUntil
        digitwords
        1
        (\i, dw ->
            if Str.startsWith line dw then
                Break i
            else
                Continue (i + 1)
        )

    if result < 10 then
        Ok result
    else
        Err NoDigit
expect startsWithDigitWord "ninerniner" == Ok 9
expect startsWithDigitWord "none" == Err NoDigit

startsWithDigitNum : Str -> Result I64 [NoDigit]
startsWithDigitNum = \line ->
    firstChar : Result U8 [ListWasEmpty]
    firstChar =
        line
        |> Str.toUtf8
        |> List.first

    firstChar
    |> Result.mapErr (\_ -> NoDigit)
    |> Result.try
        (\c ->
            if isDigit c then
                Ok ((Num.toI64 c) - 0x30)
            else
                Err NoDigit
        )
expect startsWithDigitNum "3bob" == Ok 3
expect startsWithDigitNum "bob3bob" == Err NoDigit
expect startsWithDigitNum "threebob" == Err NoDigit

startsWithDigit : Str -> Result I64 [NoDigit]
startsWithDigit = \line ->
    startsWithDigitNum line
    |> Result.onErr (\_ -> startsWithDigitWord line)

expect startsWithDigit "2wow" == Ok 2
expect startsWithDigit "twow" == Ok 2
expect startsWithDigit "wow" == Err NoDigit

strTail : Str -> Str
strTail = \str ->
  Str.toUtf8 str
  |> List.dropFirst 1
  |> Str.fromUtf8
  |> Result.withDefault ""
expect strTail "abcd" == "bcd"
expect strTail "" == ""

strFront : Str -> Str
strFront = \str ->
  Str.toUtf8 str
  |> List.dropLast 1
  |> Str.fromUtf8
  |> Result.withDefault ""
expect strFront "abcd" == "abc"
expect strFront "" == ""

lineFirstDigit : Str -> Result I64 [NoDigit]
lineFirstDigit = \line ->
  when (line, startsWithDigit line) is
    ("", _) -> Err NoDigit
    (_, Ok i) -> Ok i
    (_, _) -> lineFirstDigit (strTail line)
expect lineFirstDigit "abe123" == Ok 1
expect lineFirstDigit "abetwo" == Ok 2
expect lineFirstDigit "abekat" == Err NoDigit

lineLastDigit : Str -> Result I64 [NoDigit]
lineLastDigit = \line ->
  if line == "" then
    Err NoDigit
  else
    when lineLastDigit (strTail line) is
        Ok i -> Ok i
        Err _ -> startsWithDigit line
expect lineLastDigit "onetwothree" == Ok 3
expect lineLastDigit "onetwo3" == Ok 3
expect lineLastDigit "wowtwow" == Ok 2
expect lineLastDigit "wow" == Err NoDigit

numFromLine2 : Str -> I64
numFromLine2 = \line ->
  fd = lineFirstDigit line |> Result.withDefault 0
  ld = lineLastDigit line |> Result.withDefault 0
  (fd * 10) + ld
expect numFromLine2 "wow7one" == 71
expect numFromLine2 "wow7Bob" == 77

solvePartTwo : Str -> I64
solvePartTwo = \in ->
    in
    |> Str.trim
    |> Str.split "\n"
    |> List.map numFromLine2
    |> List.sum
expect solvePartTwo testinput2 == 281

main : Task.Task {} I32
main =
    # Stdout.line (solvePartOne input |> Num.toStr)
    Stdout.line (solvePartTwo input |> Num.toStr)
