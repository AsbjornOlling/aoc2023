app "day09"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
        parser: "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br",
        array2d: "https://github.com/mulias/roc-array2d/releases/download/v0.1.0/ssMT0bDIv-qE7d_yNUyCByGQHvpNkQJZsGUS6xEFsIY.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await },
        parser.Core.{ Parser, sepBy1, oneOf, many, map },
        parser.String.{ RawStr, parseStr, codeunit },
        array2d.Array2D.{ Array2D },
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str,
    ]
    provides [main] to pf

MirrorDir : [SWtoNE, NWtoSE]

SplitterDir : [Horizontal, Vertical]

LaserDir : [North, East, South, West]

Tile : [
    Empty,
    Mirror MirrorDir,
    Splitter SplitterDir,
]

Grid : Array2D Tile

PuzzleInput : Grid

Coords : { x : I16, y : I16 }

# toIndex : Coords -> Area2D.Index
toIndex = \{ x, y } -> {
    x: Num.toNat x,
    y: Num.toNat y,
}

parseTile : Parser RawStr Tile
parseTile =
    oneOf [
        codeunit '\\' |> map \_ -> Mirror NWtoSE,
        codeunit '/' |> map \_ -> Mirror SWtoNE,
        codeunit '|' |> map \_ -> Splitter Vertical,
        codeunit '-' |> map \_ -> Splitter Horizontal,
        codeunit '.' |> map \_ -> Empty,
    ]

parseInput : Parser RawStr PuzzleInput
parseInput =
    sepBy1 (many parseTile) (codeunit '\n')
    |> map \lsts -> Array2D.fromLists lsts FitShortest |> Array2D.transpose

parse = \in -> parseStr parseInput (Str.trim in)

expect parse input |> Result.isOk
expect
    parse testinput |> Result.isOk

move1 : Coords, LaserDir -> Coords
move1 = \pos, dir ->
    when dir is
        East -> { pos & x: pos.x + 1 }
        South -> { pos & y: pos.y + 1 }
        West -> { pos & x: pos.x - 1 }
        North -> { pos & y: pos.y - 1 }

applyMirror : LaserDir, MirrorDir -> LaserDir
applyMirror = \ldir, mdir ->
    when (ldir, mdir) is
        (East, SWtoNE) -> North
        (South, SWtoNE) -> West
        (West, SWtoNE) -> South
        (North, SWtoNE) -> East
        (East, NWtoSE) -> South
        (South, NWtoSE) -> East
        (West, NWtoSE) -> North
        (North, NWtoSE) -> West

LaserState : {
    ldir : LaserDir,
    here : Coords,
}

step : Grid, LaserState -> Set LaserState
step = \grid, { ldir, here } ->
    when Array2D.get grid (toIndex here) is
        Err OutOfBounds -> Set.empty {}
        Ok Empty -> Set.single { ldir, here: move1 here ldir }
        Ok (Mirror mirrordir) ->
            newldir = applyMirror ldir mirrordir
            Set.single { ldir: newldir, here: move1 here newldir }

        Ok (Splitter splitterdir) ->
            splitAction =
                when (ldir, splitterdir) is
                    (East, Vertical) -> Split North South
                    (West, Vertical) -> Split North South
                    (North, Horizontal) -> Split East West
                    (South, Horizontal) -> Split East West
                    _ -> Nothing
            when splitAction is
                Nothing -> Set.single { ldir, here: move1 here ldir }
                Split oneDir otherDir ->
                    Set.fromList [
                        { ldir: oneDir, here },
                        { ldir: otherDir, here },
                    ]

stepUntilDead : Grid -> Set LaserState
stepUntilDead = \grid -> runLaser grid { ldir: East, here: { x: 0, y: 0 } }

runLaserAndScore = \grid, initLaser ->
   runLaser grid initLaser
   |> Set.map .here
   |> Set.keepIf \coords -> Array2D.hasIndex grid (toIndex coords)
   |> Set.len

runLaser : Grid, LaserState -> Set LaserState
runLaser = \grid, initLaser ->
    recur : Set LaserState, Set LaserState -> Set LaserState
    recur = \visited, lasers ->
        if Set.isEmpty lasers then
            visited
        else
            # step all lasers
            steppedlasers = Set.joinMap lasers (\l -> step grid l)
            # remove already visited states
            nextlasers = Set.dropIf steppedlasers \l -> Set.contains visited l
            # add new ones to visited set
            nextvisited = Set.union visited nextlasers
            # letsgo
            recur nextvisited nextlasers

    recur (Set.single initLaser) (Set.single initLaser)

solvePartOne = \grid ->
    runLaserAndScore grid { ldir: East, here: { x: 0, y: 0 } }

expect parse (testinput) |> Result.map solvePartOne == Ok 46
expect parse (input) |> Result.map solvePartOne == Ok 7074

# lets go part 2

startingRanges : Grid -> Set LaserState
startingRanges = \in ->
    # bip
    shape = Array2D.shape in
    xs = List.range { start: At 0, end: Before shape.dimX }
    ys = List.range { start: At 0, end: Before shape.dimY }
    # boop beep
    leftside = List.map ys \y -> { ldir: East, here: { x: 0, y: Num.toI16 y } }
    rightside = List.map ys \y -> { ldir: West, here: { x: Num.toI16 (shape.dimX - 1), y: Num.toI16 y } }
    topside = List.map xs \x -> { ldir: South, here: { x: Num.toI16 x, y: 0 } }
    bottomside = List.map xs \x -> { ldir: South, here: { x: Num.toI16 x, y: Num.toI16 (shape.dimY - 1) } }
    # beep boop
    List.join [leftside, rightside, topside, bottomside]
    |> Set.fromList

solvePartTwo = \grid ->
    startingRanges grid
    |> Set.map \laser -> (runLaserAndScore grid laser)
    |> Set.toList
    |> List.max
    |> Result.withDefault 0

expect parse (testinput) |> Result.map solvePartTwo == Ok 51
expect parse (input) |> Result.map solvePartTwo == Ok 7530

main : Task.Task {} I32
main =
    when parse input is
        Err _ -> Task.err -1
        Ok in ->
            p1 = solvePartOne in
            _ <- await (Stdout.line "part one: \(Num.toStr p1)")

            p2 = solvePartTwo in
            Stdout.line "part two: \(Num.toStr p2)"
