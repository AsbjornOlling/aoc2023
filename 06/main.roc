app "day06"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
        parser: "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await },
        parser.Core.{ Parser, map, many, const, skip, apply, sepBy1 },
        parser.String.{ digits, RawStr, string, parseStr, codeunit },
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str,
    ]
    provides [main] to pf

PuzzleInput : {
    time : List U64,
    distance : List U64
}

whitespace = many (codeunit ' ')

parseNumbers = sepBy1 (map digits Num.toU64) whitespace

parseInput : Parser RawStr PuzzleInput
parseInput =
    const (\time -> \distance -> { time, distance })
    |> skip (string "Time:")
    |> skip whitespace
    |> apply parseNumbers
    |> skip (codeunit '\n')
    |> skip (string "Distance:")
    |> skip whitespace
    |> apply parseNumbers

parse = \in -> parseStr parseInput (Str.trim in)
expect Result.isOk (parse input)
expect Result.isOk (parse testinput)

# lol wolfram alpha ftw
# (I don't remember my parabola math very well)
zeros : U64, U64 -> (U64, U64)
zeros = \allowedTime, distanceRecord ->
  timeF = Num.toF64 allowedTime
  distF = Num.toF64 (distanceRecord + 1)
  lower = (timeF - Num.sqrt (timeF^2 - 4 * distF))/2
  upper = (timeF + Num.sqrt (timeF^2 - 4 * distF))/2
  dbg T lower upper
  ( Num.ceiling lower, Num.floor upper )

expect
  out = zeros 7 9
  out == (2,5)

expect
  out = zeros 15 40
  out == (4,11)

expect
  out = zeros 30 200
  out == (11, 19)

waysToWin : U64, U64 -> U64
waysToWin = \allowedTime, distanceRecord ->
    when zeros allowedTime distanceRecord is
        (lower, upper) -> 1 + (Num.min upper allowedTime) - lower

expect
  out = waysToWin 7 9
  out == 4

expect
  out = waysToWin 15 40
  out == 8

solvePartOne : PuzzleInput -> U64
solvePartOne = \in ->
    res = List.map2 in.time in.distance waysToWin
    dbg res
    List.product res

expect
  p1 = parse testinput |> Result.map solvePartOne
  p1 == Ok 288

toPartTwo = \lst ->
    numStr =
        List.map lst Num.toStr
        |> Str.joinWith ""

    when Str.toU64 numStr is
        Ok num -> num
        _ -> crash numStr

solvePartTwo : PuzzleInput -> U64
solvePartTwo = \in ->
    waysToWin (toPartTwo in.time) (toPartTwo in.distance)

main : Task.Task {} I32
main =
    when parse input is
        Err _ -> Task.err -1
        Ok in ->
            p1 = solvePartOne in
            _ <- await (Stdout.line "part one: \(Num.toStr p1)")

            p2 = solvePartTwo in
            Stdout.line "part two: \(Num.toStr p2)"
