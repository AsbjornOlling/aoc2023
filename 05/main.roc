app "day05"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
        parser: "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await },
        parser.Core.{ Parser, map, many, const, skip, apply, sepBy1, chompUntil, oneOf },
        parser.String.{ digits, RawStr, string, parseStr, codeunit },
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str,
    ]
    provides [main] to pf


# yeah I really messed up naming things today
Map : (Nat, Nat, Nat)

Mapping : {
  from : Str,
  to : Str,
  maps : List Map,
}

parseMap : Parser RawStr Map
parseMap =
    const (\a -> \b -> \c -> (a, b, c))
    |> apply digits
    |> skip (codeunit ' ')
    |> apply digits
    |> skip (codeunit ' ')
    |> apply digits

expect
    out = parseStr parseMap "123 456 789"
    out == Ok (123, 456, 789)

mapToStr : Parser RawStr (List U8) -> Parser RawStr Str
mapToStr = \p ->
    map p \u8 -> Str.fromUtf8 u8 |> Result.withDefault ""

parseMapping : Parser RawStr Mapping
parseMapping =
    const (\from -> \to -> \maps -> { from, to, maps })
    |> apply (chompUntil '-' |> mapToStr)
    |> skip (string "-to-")
    |> apply (chompUntil ' ' |> mapToStr)
    |> skip (string " map:\n")
    |> apply (sepBy1 parseMap (codeunit '\n'))

expect
    out = parseStr parseMapping "a-to-b map:\n1 2 3\n4 5 6"
    out == Ok { from: "a", to: "b", maps: [(1,2,3), (4,5,6)] }

PuzzleInput : {
   seeds : List Nat,
   mappings : List Mapping,
}

parseInput : Parser RawStr PuzzleInput
parseInput =
    const \seeds -> \mappings -> { seeds, mappings }
    |> skip (string "seeds: ")
    |> apply (sepBy1 digits (codeunit ' '))
    |> skip (string "\n\n")
    |> apply (sepBy1 parseMapping (many (codeunit '\n')))
    |> skip (many (oneOf [codeunit ' ', codeunit '\n']))

parse = \in ->
  parseStr parseInput in

expect
    out = parseStr parseInput input
    out2 = parseStr parseInput testinput
    Result.isOk out && Result.isOk out2

# ok that's parsing lets do part 1

makeMapf : Map -> (Nat -> Nat)
makeMapf = \(outstart, instart, len) ->
    \x ->
        if x < instart || instart + len < x then
            x # x is not in this range
        else
            # add first to avoid underflow
            (x + outstart) - instart

expect (makeMapf (52, 50, 48)) 79 == 81


makeMapsf : List Map -> (Nat -> Nat)
makeMapsf = \maps ->
    fs : List (Nat -> Nat)
    fs = List.map maps makeMapf

    # lol let's fold a function
    List.walk fs (\x -> x) \outf, f ->
        \x ->
            r = f x
            if r != x then
                r
            else
                outf x

expect
    f = makeMapsf [(50, 98, 2), (52, 50, 48)]
    out = f 79
    out == 81

# applyAll : a, List (a -> a) -> a
applyAll : Nat, List (Nat -> Nat) -> Nat
applyAll = \x, fs ->
   when fs is
       [f, .. as rest] ->
           r = f x
           applyAll r rest
       [] -> x

expect
  out = applyAll 2 [\x -> x + 1, \x -> x * 2]
  out == 6

solvePartOne : PuzzleInput -> Nat
solvePartOne = \in ->
  # shoop da woop
  fs : List (Nat -> Nat)
  fs = in.mappings
       |> List.map .maps
       |> List.map makeMapsf

  # hey this crashed the roc compiler
  # allMapsf = List.walk fs (\x -> x) \accf, f ->
  #     \x -> f (accf x)
  # I replaced it with `applyAll`

  allMapsf : Nat -> Nat
  allMapsf = \n -> applyAll n fs

  locationNumbers : List Nat
  locationNumbers = List.map in.seeds allMapsf

  when List.min locationNumbers is
      Ok answer -> answer
      Err _ -> crash "very not possible"

expect
  out = parse testinput |> Result.map solvePartOne
  out == Ok 35

expect
  out = parse input |> Result.map solvePartOne
  out == Ok 600279879

# ok that was part one

# part two lets gooo

formsChain : Map, Map -> Bool
formsChain = \(aStart, _, aLen), (_, bStart, bLen) ->
  aEnd = aStart + aLen
  bEnd = bStart + bLen
  (aStart <= bStart && aEnd <= bEnd) || (bStart <= aStart && bEnd <= aEnd)

expect formsChain (100, 0, 10) (99999, 100, 10)

sortByInStart : List Map -> List Map
sortByInStart = \maps ->
  List.sortWith maps \(_, a, _), (_, b, _) ->
    if a < b then
      LT
    else if a > b then
      GT
    else
      EQ

addMissingMaps : List Map -> List Map
addMissingMaps = \maps ->
  sorted = sortByInStart maps

  # put new ranges in between
  middle : List (List (Nat, Nat, Nat))
  middle =
    List.map2
      (List.dropLast sorted 1)
      (List.dropFirst sorted 1)
      \a, b ->
        aStart = a.1
        aLen = a.2
        bStart = b.1
        if aStart + aLen == bStart then
          # perfect join, don't add anyting
          [a, b]
        else if aStart + aLen < bStart then
          # add a missing range
          newrange : (Nat, Nat, Nat)
          newrange = (
            (aStart + aLen) |> Num.toNat,
            (aStart + aLen) |> Num.toNat,
            (bStart - (aStart + aLen)) |> Num.toNat
          )
          [a, newrange, b]
        else
          crash "assumption failed: ranges do overlap"

  # the range from zero to the first element
  expect Result.isOk (List.get middle 0)
  before : Result (Nat, Nat, Nat) [ListWasEmpty, AlreadyStartsAtZero]
  before =
    List.first sorted
    |> Result.try \(_, start, _) ->
        if start == 0 then
            Err AlreadyStartsAtZero
        else
            Ok (0, 0, start)

  after : Result (Nat, Nat, Nat) [ListWasEmpty]
  after =
    List.last sorted
    |> Result.map \(_, start, len) ->
        (start + len |> Num.toNat,
         start + len |> Num.toNat,
         (Num.toNat Num.maxU64) - (start + len))

  beforeL = Result.map before List.single |> Result.withDefault []
  afterL = Result.map after List.single |> Result.withDefault []

  middle
  |> List.prepend beforeL
  |> List.append afterL
  |> List.join


expect
  out = addMissingMaps [(0, 1, 10), (0, 20, 10)]
  out == [(0, 0, 1), (0, 1, 10), (11, 11, 9), (0, 20, 10), (30, 30, Num.maxU64 - 30 |> Num.toNat)]


#/ combineMaps : Map, Map -> List Map
# combineMaps = \(aOutStart, aInStart, aLen), (bOutStart, bInStart, bLen) ->
#    ## aOut--------------aOut+aLen
#    ##       bIn--------------bIn+bLen
#    if aOutStart <= bStart && aOutStart + aLen <= bInStart + bLen then
#        # b's input range starts inside a's output range
#        overlapLen = (bInStart + bLen) - (aOutStart + aLen)
#        [ # the bit of a's mapping before b
#          (aOutStart, aInStart, bStart - aOutStart)
#          # the bit of b's mapping that is hit by a
#          (bOutStart, (bInStart + aOutStart) - aInStart, bLen - overlapLen)
#          # the bit of b's mapping that is after a
#          (bOutStart, aOutStart+aLen, bLen - overlapLen)
#        ]
#    else
#        crash "unimplemented"
#
# expect
#     out = combineMaps (10, 0, 10) (25, 15, 10)
#     out == [(10,0,5), (25, 5, 5), (10, 10, 5)]

# combineMappings : Mapping, Mapping -> Mapping
# combineMappings = \earlier, later ->
#     expect earlier.to == later.from
#
#     newmaps : List Map
#     newmaps =
#         List.map earlier.maps \earliermap ->
#             List.map later.maps \latermap ->
#                 combineMaps earliermap latermap
#
#     # a rgh this todays problem is dumb and i hate it
#     { from : earlier.from
#     , to : earlier.to
#     , maps : newmaps }


solvePartTwo = \_ ->
  # patchedInput : PuzzleInput
  # patchedInput = { in &
  #   mappings : List.map in.mappings \mapping ->
  #     { mapping & maps : addMissingMaps mapping.maps }
  # }

  # expect List.len patchedInput.mappings > 0
  # lastLayerSorted : List Map
  # lastLayerSorted =
  #     List.last patchedInput.mappings
  #     |> Result.map \last -> sortByInStart last.maps
  #     |> Result.withDefault []

  0

main : Task.Task {} I32
main =
    when parse input is
        Err _ -> Task.err -1
        Ok in ->
            p1 = solvePartOne in
            _ <- await (Stdout.line "part one: \(Num.toStr p1)")

            p2 = solvePartTwo in
            Stdout.line "part two: \(Num.toStr p2)"
