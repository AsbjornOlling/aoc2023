app "hello"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
        parser: "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await },
        parser.Core.{ Parser, many, const, skip, apply, sepBy1, ignore },
        parser.String.{ digits, RawStr, string, parseStr},
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str
    ]
    provides [main] to pf


Card : {
  id : Nat,
  myNums: List Nat,
  winNums: List Nat
}

spaces : Parser RawStr {}
spaces = many (string " ") |> ignore

parseCard : Parser RawStr Card
parseCard =
    const (\id -> \winNums -> \myNums -> { id, myNums, winNums })
    |> skip (string "Card")
    |> skip spaces
    |> apply (digits)
    |> skip (string ":")
    |> skip spaces
    |> apply (sepBy1 digits spaces)
    |> skip spaces
    |> skip (string "|")
    |> skip spaces
    |> apply (sepBy1 digits spaces)

expect
  in = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"
  out = parseStr parseCard in
  id = 1
  winNums = [41, 48, 83, 86, 17]
  myNums = [83, 86,  6, 31, 17,  9, 48, 53]
  out == Ok {id, winNums, myNums}

expect
  in = "Card   1:  9 32  7 82 10 36 31 12 85 95 |  7 69 23  9 32 22 47 10 95 14 24 71 57 12 31 59 36 68  2 82 38 80 85 21 92"
  out = parseStr parseCard in
  Result.isOk out

parseInput : Parser RawStr (List Card)
parseInput =
    sepBy1 parseCard (string "\n")


parse : Str -> Result (List Card) [ParsingFailure Str, ParsingIncomplete Str]
parse = \in ->
    parseStr parseInput (Str.trim in)

# parsing done - lets do part one

cardPoints : Card -> Nat
cardPoints = \card ->
    List.walk card.myNums 0 \points, myNum ->
        isWin = List.contains card.winNums myNum
        if isWin && points == 0 then
          1
        else if isWin then
          points * 2
        else
          points
expect
    in = { id : 1, myNums : [1, 2, 3, 4], winNums: [2, 3, 4, 5]}
    out = cardPoints in
    out == 4

solvePartOne : (List Card) -> Nat
solvePartOne = \cards ->
  cards
  |> List.map cardPoints
  |> List.sum

expect
  out = parse testinput |> Result.map solvePartOne
  out == Ok 13

# part one done - lets do part 2

countWins : Card -> Nat
countWins = \card ->
  List.countIf card.myNums \num -> List.contains card.winNums num

solvePartTwo : List (Card) -> Nat
solvePartTwo = \cards ->
  countsBefore : Dict Nat Nat
  countsBefore = List.map cards (\c -> (c.id, 1)) |> Dict.fromList

  incr : Dict Nat Nat, List Nat, Nat -> Dict Nat Nat
  incr = \indict, range, incrBy ->
    List.walk range indict \d, i ->
      Dict.update d i \maybeV ->
        when maybeV is
          Present v -> Present (v + incrBy)
          Missing -> Missing

  countsAfter : Dict Nat Nat
  countsAfter = List.walk cards countsBefore \counts, card ->
    wins = countWins card
    incrRange = List.range { start: After card.id, end: Length wins }
    incr counts incrRange (Dict.get counts card.id |> Result.withDefault 999) # TODO: remove this?

  Dict.values countsAfter |> List.sum

expect
  out = parse testinput |> Result.map solvePartTwo
  out == Ok 30

main : Task.Task {} I32
main =
    when parse input is
        Ok cards ->
            p1 = solvePartOne cards
            _ <- await (Stdout.line "\(Num.toStr p1)")

            p2 = solvePartTwo cards
            Stdout.line "\(Num.toStr p2)"
        Err e ->
            dbg e
            Stdout.line "Failed parsing :("
