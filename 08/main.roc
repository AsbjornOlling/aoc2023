app "day08"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
        parser: "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await },
        parser.Core.{ Parser, map, const, skip, apply, sepBy1, oneOf, many, flatten },
        parser.String.{ RawStr, parseStr, codeunit, anyCodeunit, string, anyCodeunit },
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str,
    ]
    provides [main] to pf

Direction : [Left, Right]

Instructions : List Direction

Node : Str

Graph : Dict Node (Node, Node)

PuzzleInput : {
    instructions: Instructions,
    graph : Graph
}

parseDirection : Parser RawStr Direction
parseDirection =
    oneOf [ codeunit 'L' |> map \_ -> Left
          , codeunit 'R' |> map \_ -> Right
          ]

parseInstructions : Parser RawStr Instructions
parseInstructions =
    many parseDirection

parseNode : Parser RawStr Node
parseNode =
    const \a -> \b -> \c -> [a,b,c]
    |> apply anyCodeunit
    |> apply anyCodeunit
    |> apply anyCodeunit
    |> map \lst -> Str.fromUtf8 lst |> Result.mapErr (\_ -> "could not make string from node name")
    |> flatten

parseGraphLine : Parser RawStr (Node, (Node, Node))
parseGraphLine =
    const \a -> \b -> \c -> (a, (b, c))
    |> apply parseNode
    |> skip (string " = (")
    |> apply parseNode
    |> skip (string ", ")
    |> apply parseNode
    |> skip (string ")")

parseGraph : Parser RawStr Graph
parseGraph =
    sepBy1 parseGraphLine (codeunit '\n')
    |> map Dict.fromList

parsePuzzleInput : Parser RawStr PuzzleInput
parsePuzzleInput =
    const \instructions -> \graph -> { instructions, graph }
    |> apply parseInstructions
    |> skip (string "\n\n")
    |> apply parseGraph

parse = \in -> parseStr parsePuzzleInput (Str.trim in)

expect Result.isOk (parse input)
expect Result.isOk (parse testinput)

# ok that was parsing - lets do part 1

rotate : List a -> List a
rotate = \lst ->
    when lst is
        [a, .. as rest] -> List.append rest a
        [] -> []

expect rotate [1,2,3] == [2,3,1]

findNextNode = \g, instructions, thisNode ->
    when (Dict.get g thisNode, instructions) is
        (Ok (next, _), [Left, ..]) -> next
        (Ok (_, next), [Right, ..]) -> next
        _ -> crash "impossible case: empty instructions or missing node"

distanceToZZZ : Graph, Instructions, Node, U32 -> U32
distanceToZZZ = \g, instructions, thisNode, distanceSoFar ->
    if thisNode == "ZZZ" then
        distanceSoFar
    else
        distanceToZZZ g
            (rotate instructions)
            (findNextNode g instructions thisNode)
            (distanceSoFar + 1)


solvePartOne : PuzzleInput -> U32
solvePartOne = \in ->
    distanceToZZZ in.graph in.instructions "AAA" 0

expect parse input |> Result.map solvePartOne == Ok 16897


# ok that was part one - part two here we come

# something something figure out the Z-period of each path we take
# surely all paths loop eventually, right?
# we could find the path it takes before looping
# and then figure out where all the z indices are
# and then uhh buhhh math LCM what?


findLoop : Graph, Instructions, Node -> (List Node, U32)
findLoop = \g, startInstrs, startNode ->
    findLoopInner = \instrs, thisNode, path, visited, distance ->
        here = (thisNode, instrs, distance)
        beenHereBefore = List.any visited \(tn, irs, _) ->
            thisNode == tn && instrs == irs

        if beenHereBefore then
            (path, distance)
        else
            findLoopInner
                (rotate instrs)
                (findNextNode g instrs thisNode)
                (List.append path thisNode)
                (List.append visited here)
                (distance + 1)

    findLoopInner startInstrs startNode [] [] 0


solvePartTwo : PuzzleInput -> U32
solvePartTwo = \in ->
    startNodes = List.keepIf (Dict.keys in.graph) \k ->
        List.last (Str.toUtf8 k) == Ok 'A'

    loopPaths = List.map startNodes \startNode ->
        findLoop in.graph in.instructions startNode

    dbg "okay here"

    x = List.first loopPaths |> Result.map \_ -> "wow"

    dbg "okay there"

    dbg x
    dbg "never here"

    # just to prevent this from being optimized away
    List.len loopPaths |> Num.toU32

# neat!

main : Task.Task {} I32
main =
    when parse testinput is
        Err _ -> Task.err -1
        Ok in ->
            p1 = solvePartOne in
            _ <- await (Stdout.line "part one: \(Num.toStr p1)")

            p2 = solvePartTwo in
            Stdout.line "part two: \(Num.toStr p2)"
