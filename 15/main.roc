app "day09"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
        parser: "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await },
        parser.Core.{ Parser, const, skip, apply, sepBy1, oneOf, many, map, flatten },
        parser.String.{ RawStr, parseStr, codeunit, digits, codeunitSatisfies },
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str,
    ]
    provides [main] to pf


Label : Str

FocalLength : Nat

Lens : { length : FocalLength, label : Label }

Operation : [
  Place Label FocalLength,
  Remove Label,
]

isLetter = \b -> 'a' <= b && b <= 'z'

parseLabel : Parser RawStr Label
parseLabel =
  many (codeunitSatisfies isLetter)
  |> map (\xs -> Str.fromUtf8 xs |> Result.mapErr \_ -> "bad string")
  |> flatten

#parsePlaceOp : Parser RawStr [Place Label FocalLength]
parsePlaceOp : Parser RawStr Operation
parsePlaceOp =
  const \label -> \len -> Place label len
  |> apply parseLabel
  |> skip (codeunit '=')
  |> apply digits

# parseRemoveOp : Parser RawStr [Remove Label]
parseRemoveOp : Parser RawStr Operation
parseRemoveOp =
  const \label -> Remove label
  |> apply parseLabel
  |> skip (codeunit '-')

parseOp : Parser RawStr Operation
parseOp = oneOf [parsePlaceOp, parseRemoveOp]

parseInput : Parser RawStr (List Operation)
parseInput =
  sepBy1 parseOp (codeunit ',')

parse2 = \in ->
  parseStr parseInput (Str.trim in)

expect
  out = parse2 testinput
  Result.isOk out

parse = \in ->
  Ok (Str.trim in |> Str.split ",")

hash : Str -> U8
hash = \s ->
  init : U8
  init = 0
  Str.walkUtf8 s init \curr, char ->
    Num.toU8 ((((Num.toI64 curr) + (Num.toI64 char)) * 17) % 256)

expect hash "HASH" == 52

solvePartOne = \in ->
  in
  |> List.map hash
  |> List.sum

# expect parse testinput |> Result.map solvePartOne == Ok 1320

replace : List Lens, Lens -> List Lens
replace = \lst, replaceWith ->
  List.walk lst [] \acc, lens ->
    if lens.label == replaceWith.label then
      List.append acc replaceWith
    else
      List.append acc lens

solvePartTwo : List Operation -> Nat
solvePartTwo = \in ->
  initState : Dict U8 (List Lens)
  initState = Dict.empty {}

  results = List.walk in initState \state, op ->
    when op is
      Remove label ->
        Dict.update state (hash label) \result ->
          when result is
            Missing -> Missing
            Present v ->
              Present (List.dropIf v \k -> k.label == label)

      Place label length ->
        Dict.update state (hash label) \result ->
          when result is
            Missing -> Present [ { length, label } ]
            Present vs ->
              if List.any vs \v -> v.label == label then
                Present (replace vs { label, length })
              else
                Present (List.append vs { label, length })
  dbg results
  focusingPower results


focusingPower : Dict U8 (List Lens) -> Nat
focusingPower = \dict ->
  Dict.walk dict 0 \result, boxnum, lenses ->
    newPower =
      List.mapWithIndex lenses \lens, lensSlot ->
        (1 + Num.toNat boxnum) * (1 + lensSlot) * lens.length
      |> List.sum
    result + newPower

main : Task.Task {} I32
main =
    #p1 = solvePartOne (parse input)
    #_ <- await (Stdout.line "part one: \(Num.toStr p1)")

    p2in = parse2 input
    when p2in is
      Ok in ->
        p2 = solvePartTwo in
        Stdout.line "part two: \(Num.toStr p2)"
      Err _ ->
        crash "FUCK"
