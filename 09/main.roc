app "day09"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
        parser: "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await },
        parser.Core.{ Parser, const, skip, apply, sepBy1, oneOf, },
        parser.String.{ RawStr, parseStr, codeunit, digits },
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str,
    ]
    provides [main] to pf


PuzzleInput : List (List I64)

parseNumber : Parser RawStr I64
parseNumber = const Num.toI64 |> apply digits

parseNegativeNumber : Parser RawStr I64
parseNegativeNumber =
  const \num -> -1 * (Num.toI64 num)
  |> skip (codeunit '-')
  |> apply digits

parseLine : Parser RawStr (List I64)
parseLine = sepBy1 (oneOf [parseNumber, parseNegativeNumber]) (codeunit ' ')

parseInput : Parser RawStr PuzzleInput
parseInput = sepBy1 parseLine (codeunit '\n')

parse = \in -> parseStr parseInput (Str.trim in)

expect Result.isOk (parse testinput)
expect Result.isOk (parse input)

# parsing done

distsBetween = \lst ->
  List.map2 (List.dropFirst lst 1) (List.dropLast lst 1) Num.sub

extrapolate = \lst ->
  if List.all lst \x -> x == 0 then
    0
  else
    valueBelow = extrapolate (distsBetween lst)
    when List.last lst is
      Ok last -> valueBelow + last
      Err _ -> crash "oh fuck"

expect extrapolate [0, 3, 6, 9, 12, 15] == 18

expect extrapolate [45, 30, 21, 16, 13, 10] == 5

solvePartOne = \in ->
  in
  |> List.map extrapolate
  |> List.sum

expect parse testinput |> Result.map solvePartOne == Ok 114
expect parse input |> Result.map solvePartOne == Ok 1887980197

# lol that's ridiculous

solvePartTwo = \in ->
  in
  |> List.map List.reverse
  |> List.map extrapolate
  |> List.sum

expect parse input |> Result.map solvePartTwo == Ok 990

main : Task.Task {} I32
main =
    when parse input is
        Err _ -> Task.err -1
        Ok in ->
            p1 = solvePartOne in
            _ <- await (Stdout.line "part one: \(Num.toStr p1)")

            p2 = solvePartTwo in
            Stdout.line "part two: \(Num.toStr p2)"
