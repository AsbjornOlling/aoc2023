{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    roc.url = "github:roc-lang/roc";
    roc2nix = {
      url = "github:JRMurr/roc2nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.roc.follows = "roc";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, roc, roc2nix, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };

        # For now you need to manually set the roc cli when you get the lib
        rocPkgs = roc.packages.${system};
        rocLib = (roc2nix.lib.${system}).overrideToolchain rocPkgs.cli;
        rocDeps = {
          "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br" =
            "sha256-71jf2j6/0wsAuEKVJU+VJ+XwBNWvFvX6IkR3gjfbkX8=";
          "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br" =
            "sha256-JDD9F9YbyYVervw2tZp3L6y0JO6sGWG6lp6w0npAObg=";
        };

        buildDay = day:
          rocLib.buildRocApp {
            name = "aoc-2023-${day}";
            version = "aoc2023";
            src = ./${day}/.;
            inherit rocDeps;
          };

        testDay = day:
          rocLib.buildRocApp {
            name = "aoc-2023-${day}-test";
            version = "aoc2023";
            src = ./${day}/.;
            buildPhaseRocCommand = "roc test main.roc";
            installPhaseCommand = "mkdir $out && touch $out/ok";
            dontFixup = true;
            inherit rocDeps;
          };

        allDays = fun:
          pkgs.lib.lists.foldr (elem: state: state // { "${elem}" = fun elem; })
          { } [ "01" "02" "03" "04" "05" "06" "07" ];

        buildAllDays = allDays buildDay;
        testAllDays = allDays testDay;

      in {
        formatter = pkgs.nixpkgs-fmt;

        devShells = {
          default = pkgs.mkShell {
            buildInputs = with pkgs; [ rocPkgs.cli ];
          };
        };

        packages = buildAllDays;
        checks = testAllDays;
      });
}
