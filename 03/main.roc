app "hello"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await },
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str
    ]
    provides [main] to pf

ElemPos : (U8, (Nat, Nat))

parse : Str -> List (List ElemPos)
parse = \in ->
  Str.trim in
  |> Str.split "\n"
  |> List.map Str.toUtf8
  |> List.map (\line -> line |> List.prepend '.' |> List.append '.')
  |> List.mapWithIndex \line, y ->
       List.mapWithIndex line \char, x ->
           (char, (x, y))
expect
  out : List (List ElemPos)
  out = parse "ab\ncd"
  out == [[('.', (0,0)), ('a', (1,0)), ('b', (2,0)), ('.', (3,0))],
          [('.', (0,1)), ('c', (1,1)), ('d', (2,1)), ('.', (3,1))]]

isDigit : U8 -> Bool
isDigit = \b ->
    Bool.and (0x30 <= b) (b <= 0x39)

getDigit : U8 -> Result U8 [NoDigit]
getDigit = \b ->
    if isDigit b then
        Ok (Num.toU8 (b - 0x30))
    else
        Err NoDigit
expect getDigit 0x32 == Ok 2
expect getDigit 0 == Err NoDigit

numFromDigits : List U8 -> U64
numFromDigits = \digits ->
  List.walk digits (Num.toU64 0) \acc, d ->
      acc * 10 + Num.toU64 d
expect numFromDigits [1, 2, 3] == 123

numsAndNeighborsFromLine : List ElemPos, List ElemPos, List ElemPos -> List (U64, List ElemPos)
numsAndNeighborsFromLine = \lineBefore, line, lineAfter ->
    # get all of the nums from `line`, and their neighbors

    # get neighbors of a completed string of digits
    getNeighbors : Nat, Nat -> List ElemPos
    getNeighbors = \lastidx, len ->
        # lastidx is the first non-digit char
        # len is number of digits in found number
        start = lastidx
        end = lastidx - (len + 1)
        List.range { start: At start, end: At end }
        # get above and below
        |> List.map \i ->
            [ List.get lineBefore i, List.get lineAfter i ]
        |> List.join
        # and the two neighbors on the middle line
        |> List.concat [
            List.get line lastidx,
            List.get line end
          ]
        |> List.keepOks (\x -> x)

    # walk `line`, and build numbers
    initState : {results: List (U64, List ElemPos), numSoFar: List U8}
    initState = {results: [], numSoFar: []}
    List.walkWithIndex line initState \state, (char, _), idx ->
        when getDigit char is
            Ok num ->
                # nom nom
                {state & numSoFar: List.append state.numSoFar num }

            Err NoDigit ->
                if state.numSoFar != [] then
                  # we reached a non-digit
                  # store the number we were building and reset
                  { state & numSoFar: [],
                            results: List.append state.results
                                     ( numFromDigits state.numSoFar
                                     , getNeighbors idx (List.len state.numSoFar)
                                     )
                  }
                else
                  # nothing after nothing... just continue
                  state
    |> .results


numsAndNeighbors : List (List ElemPos) -> List (U64, List ElemPos)
numsAndNeighbors = \grid ->
  lines = grid

  linesBefore = List.dropLast lines 1 |> List.prepend []
  linesAfter = List.dropFirst lines 1 |> List.append []
  expect (List.len linesBefore) == (List.len lines)
  expect (List.len linesAfter) == (List.len lines)
  List.map3 linesBefore lines linesAfter numsAndNeighborsFromLine
  |> List.join


solvePartOne : List (List ElemPos) -> Nat
solvePartOne = \grid ->
    nns = numsAndNeighbors grid
    hassymbol = List.keepIf nns \(_, neighbors) ->
        List.any neighbors \(c, _) -> c != '.'

    List.sum (hassymbol |> List.map .0) |> Num.toNat
expect solvePartOne (parse testinput) == 4361
expect solvePartOne (parse input) == 522726

# part two

solvePartTwo : List (List ElemPos) -> Nat
solvePartTwo = \grid ->
    nns = numsAndNeighbors grid

    cogNums : List ((Nat, Nat), U64)
    cogNums = List.keepOks nns \(num, neighbors) ->
        List.walk neighbors (Err NoCog) \state, (char, loc) ->
            when (char, state) is
                ('*', Err _) -> Ok (loc, num)
                ('*', Ok _) -> crash "num has more than one cog"
                (_, s) -> s

    cogDict : Dict (Nat, Nat) (List U64)
    cogDict = List.walk cogNums (Dict.empty {}) \dict, (loc, num) ->
       when Dict.get dict loc is
           Ok nums ->
               Dict.insert dict loc (List.append nums num)
           Err KeyNotFound ->
               Dict.insert dict loc [num]

    cogsRatiosDict : Dict (Nat, Nat) U64
    cogsRatiosDict = Dict.walk cogDict (Dict.empty {}) \dict, loc, nums ->
       when nums is
           [a, b] -> Dict.insert dict loc (a * b)
           _ -> dict

    Dict.values cogsRatiosDict |> List.sum |> Num.toNat

expect solvePartTwo (parse testinput) == 467835
expect solvePartTwo (parse input) == 81721933

main : Task.Task {} I32
main =
    grid = parse input
    p1 = solvePartOne grid
    _ <- await (Stdout.line "p1: \(Num.toStr p1)")

    p2 = solvePartTwo grid
    Stdout.line "p2: \(Num.toStr p2)"
