app "day07"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
        parser: "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await },
        parser.Core.{ Parser, map, const, skip, apply, sepBy1, oneOf, many },
        parser.String.{ digits, RawStr, parseStr, codeunit },
        "input.txt" as input : Str,
        "testinput.txt" as testinput : Str,
    ]
    provides [main] to pf

Card : U8

Hand : List Card

Game : { hand : Hand, bet: U32 }

PuzzleInput : List Game

makeCardParser : (U8, Card) -> Parser RawStr Card
makeCardParser = \(char, card) ->
    codeunit char |> map \_ -> card

cardParsers : List (Parser RawStr Card)
cardParsers = [
    ('2', 2),
    ('3', 3),
    ('4', 4),
    ('5', 5),
    ('6', 6),
    ('7', 7),
    ('8', 8),
    ('9', 9),
    ('T', 10),
    ('J', 11),
    ('Q', 12),
    ('K', 13),
    ('A', 14)
] |> List.map makeCardParser

parseCard : Parser RawStr Card
parseCard = oneOf cardParsers

parseHand : Parser RawStr Hand
parseHand = many parseCard

parseGame : Parser RawStr Game
parseGame =
    const (\hand -> \bet -> { hand, bet })
    |> apply parseHand
    |> skip (codeunit ' ')
    |> apply (map digits Num.toU32)

parseInput : Parser RawStr PuzzleInput
parseInput =
    sepBy1 parseGame (codeunit '\n')

parse = \in -> parseStr parseInput (Str.trim in)
expect Result.isOk (parse testinput)
expect Result.isOk (parse input)

# ok that was parsing lets do part 1

# just make it a U8 so we fast sorting
HandKind : U8

handKind : Hand -> HandKind
handKind = \hand ->
    expect List.len hand <= 5

    countd : Dict Card U8
    countd = List.walk hand (Dict.empty {}) \state, card ->
        Dict.update state card \value ->
            when value is
                Present count -> Present (count + 1)
                Missing -> Present 1

    counts = Dict.values countd |> List.sortDesc

    when counts is
        [5]             -> 7 # five of a kind
        [4, 1]          -> 6 # four of a kind
        [3, 2]          -> 5 # full house
        [3, 1, 1]       -> 4 # three of a kind
        [2, 2, 1]       -> 3 # two pairs
        [2, 1, 1, 1]    -> 2 # one pair
        [1, 1, 1, 1, 1] -> 1 # high card

        # lol joker rules
        # 1 joker
        [4]          -> 7 # five of a kind
        [3, 1]       -> 6 # four of a kind
        [2, 2]       -> 5 # full house
        [2, 1, 1]    -> 4 # three of a kind
        [1, 1, 1, 1] -> 2 # one pair

        # 2 jokers
        [3]       -> 7 # five of a kind
        [2, 1]    -> 6 # four of a kind
        [1, 1, 1] -> 4 # three of a kind

        # 3 jokers
        [2]    -> 7 # five of a kind
        [1, 1] -> 6 # four of a kind

        # four jokers
        [1] -> 7 # five of a kind

        # five jokers
        [] -> 7 # five of a kind

        _ -> crash "impossible hand kind"

compare = \a, b ->
   if a < b then
       LT
   else if a > b then
       GT
   else
       EQ

zip = \xs, ys -> List.map2 xs ys \x, y -> (x, y)

compareHandValues : Hand, Hand -> [LT, GT, EQ]
compareHandValues = \h1, h2 ->
    expect List.len h1 == List.len h2
    zip h1 h2
    |> List.walkUntil EQ \_, (c1, c2) ->
        when compare c1 c2 is
            EQ -> Continue EQ
            other -> Break other

compareHandKinds : Hand, Hand -> [LT, GT, EQ]
compareHandKinds = \h1, h2 ->
    compare (handKind h1) (handKind h2)

compareHands : Hand, Hand -> [LT, GT, EQ]
compareHands = \h1, h2 ->
    when compareHandKinds h1 h2 is
        EQ -> compareHandValues h1 h2
        other -> other

expect
  h1 = [1,1,1,1,1]
  h2 = [9,9,9,3,3]
  out = compareHands h1 h2
  out == GT

calculateWinnings : List Game -> U32
calculateWinnings = \sortedGames ->
    List.map2
        (List.range { start: At 1, end : Length (List.len sortedGames) })
        sortedGames
        \i, game -> i * game.bet
    |> List.sum

solvePartOne : PuzzleInput -> U32
solvePartOne = \games ->
    List.sortWith games \g1, g2 ->
        compareHands g1.hand g2.hand
    |> calculateWinnings

expect parse testinput |> Result.map solvePartOne == Ok 6440
expect parse input |> Result.map solvePartOne == Ok 253603890

# ok that was part one done
# lets do part two
# the real trick to my part two is up in `handKind`

joker : Card
joker = 11

removeJokers : Hand -> Hand
removeJokers = \hand ->
    List.dropIf hand \card -> card == joker

demoteJokers : Hand -> Hand
demoteJokers = \hand ->
    List.map hand \c -> if c == joker then 0 else c

compareHands2 : Hand, Hand -> [LT, GT, EQ]
compareHands2 = \h1, h2 ->
    when compareHandKinds (removeJokers h1) (removeJokers h2) is
        EQ -> compareHandValues (demoteJokers h1) (demoteJokers h2)
        other -> other

solvePartTwo : PuzzleInput -> U32
solvePartTwo = \games ->
    List.sortWith games \g1, g2 ->
        compareHands2 g1.hand g2.hand
    |> calculateWinnings

expect parse testinput |> Result.map solvePartTwo == Ok 5905
expect parse input |> Result.map solvePartTwo == Ok 253630098

# neat!

main : Task.Task {} I32
main =
    when parse input is
        Err _ -> Task.err -1
        Ok in ->
            p1 = solvePartOne in
            _ <- await (Stdout.line "part one: \(Num.toStr p1)")

            p2 = solvePartTwo in
            Stdout.line "part two: \(Num.toStr p2)"
