app "hello"
    packages {
        pf: "https://github.com/roc-lang/basic-cli/releases/download/0.7.0/bkGby8jb0tmZYsy2hg1E_B2QrCgcSTxdUlHtETwm5m4.tar.br",
        parser: "https://github.com/lukewilliamboswell/roc-parser/releases/download/0.2.0/dJQSsSmorujhiPNIvJKlQoI92RFIG_JQwUfIxZsCSwE.tar.br",
    }
    imports [
        pf.Stdout,
        pf.Task.{ Task, await, loop },
        pf.Utc.{ Utc },
        parser.Core.{ Parser, map, const, skip, apply, sepBy1 },
        parser.String.{ digits, RawStr, oneOf, string, parseStr },
        "input.txt" as input : Str,
        # "testinput.txt" as testinput : Str
    ]
    provides [main] to pf

Color : [Red, Green, Blue]

parseColor : Parser RawStr Color
parseColor = oneOf [
    string "red" |> map (\_ -> Red),
    string "green" |> map (\_ -> Green),
    string "blue" |> map (\_ -> Blue),
]

GameRound : { red : Nat, green : Nat, blue : Nat }

parseGrab : Parser RawStr (Nat, Color)
parseGrab =
    const (\n -> \c -> (n, c))
    |> apply digits
    |> skip (string " ")
    |> apply parseColor

countColors : List (Nat, Color) -> GameRound
countColors = \games ->
    init = { red: 0, green: 0, blue: 0 }
    List.walk games init \state, (amount, color) ->
        when color is
            Red -> { state & red: state.red + amount }
            Green -> { state & green: state.green + amount }
            Blue -> { state & blue: state.blue + amount }

parseRound : Parser RawStr GameRound
parseRound =
    sepBy1 parseGrab (string ", ")
    |> map countColors

Game : { id : Nat, rounds : List GameRound }

parseGame : Parser RawStr Game
parseGame =
    const (\id -> \rs -> { id, rounds: rs })
    |> skip (string "Game ")
    |> apply digits
    |> skip (string ": ")
    |> apply (sepBy1 parseRound (string "; "))
expect
    in = "Game 1: 1 red, 1 green; 1 red, 1 blue"
    out = parseStr parseGame in
    expected = { id: 1, rounds: [{ red: 1, green: 1, blue: 0 }, { red: 1, green: 0, blue: 1 }] }
    out == Ok expected

parseGames : Parser RawStr (List Game)
parseGames = sepBy1 parseGame (string "\n")

# phew- that was parsing
# what the heck

# part 1:

limit = { red: 12, green: 13, blue: 14 }

roundIsPossible : GameRound -> Bool
roundIsPossible = \round ->
    (round.red <= limit.red) && (round.green <= limit.green) && (round.blue <= limit.blue)

gameIsPossible : Game -> Bool
gameIsPossible = \game ->
    List.all game.rounds roundIsPossible

solvePartOne : List Game -> Nat
solvePartOne = \games ->
    List.walk games 0 \sum, game ->
        if gameIsPossible game then
            sum + game.id
        else
            sum

expect parseStr parseGames (Str.trim input) |> Result.map solvePartOne == Ok 2239

# ok part two

minCubes : Game -> { red : Nat, green : Nat, blue : Nat }
minCubes = \game ->
    maxOfColor = \cfun -> List.map game.rounds cfun |> List.max |> Result.withDefault 0
    {
        red: maxOfColor .red,
        green: maxOfColor .green,
        blue: maxOfColor .blue,
    }

power : GameRound -> Nat
power = \round ->
    round.red * round.blue * round.green

solvePartTwo : List Game -> Nat
solvePartTwo = \games ->
    List.walk games 0 \sum, game ->
        sum + power (minCubes game)

expect parseStr parseGames (Str.trim input) |> Result.map solvePartTwo == Ok 83435

# timing stuff

solveBoth : Str -> Result (Nat, Nat) [ParsingFailure Str, ParsingIncomplete Str]
solveBoth = \in ->
    parsed = parseStr parseGames (Str.trim in)
    Result.map parsed \games ->
        p1 = solvePartOne games
        p2 = solvePartTwo games
        (p1, p2)

time : (a -> b), a -> Task (b, Nat) I32
time = \fun, arg ->
  before <- await Utc.now
  result = fun arg
  after <- await Utc.now

  ns = Utc.toNanosSinceEpoch
  Num.toNat (ns after - ns before)
  |> \x -> Task.ok (result, x)

timeMany : (a -> b), a -> Task (List (b, Nat)) I32
timeMany = \fun, arg ->
  loop [] \state ->
    if List.len state < 100 then
      result <- await (time fun arg)
      next = List.append state result
      Task.ok (Step next)
    else
      Task.ok (Done state)

timeAverage : (a -> b), a -> Task F64 I32
timeAverage = \fun, arg ->
  results <- await (timeMany fun arg)
  times = List.map results \(_,t) -> Num.toF64 t
  average = List.sum times / Num.toFrac (List.len times)
  Num.toF64 average
  |> Task.ok

main : Task.Task {} I32
main =
    took <- await (timeAverage solveBoth input)
    # tookint = Num.round took

    ns =  Num.toStr took
    _ <- await (Stdout.line "took \(ns)ns on average")

    ms = Num.toStr (took / 1000000)
    Stdout.line "took \(ms)ms avg"
